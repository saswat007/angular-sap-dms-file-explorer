import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DocumentDashboardComponent } from './components/document-dashboard/document-dashboard.component';
import { FileExplorerComponent } from './components/file-explorer/file-explorer.component';


const routes: Routes = [{path : '', component : FileExplorerComponent},
{path : 'documents', component : DocumentDashboardComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
