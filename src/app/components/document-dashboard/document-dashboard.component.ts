import { Component, OnInit } from '@angular/core';

export interface Document {
  crewId: string;
  documentType: string;
  createdOn: string;
  countryOfIssue: string;
  changedBy: string;
  lastName: string;
  firstName: string;
  middleName: string;
  dateOfBirth: string;
  gender: string;
  subDocumentType: string;
  documentNumber: string;
  passportNumber: string;
  placeOfIssue: string;
  cityOfBirth: string;
  stateOfBirth: string;
  countryOfBirth: string;
  dateOfIssue: string;
  dateOfExpiration: string;
  nationality: string;
  primaryPassport: string;
  validIn: string;
  entry: string;
  dMSObjectId: string;
  invalidData: string;
}

@Component({
  selector: 'app-document-dashboard',
  templateUrl: './document-dashboard.component.html',
  styleUrls: ['./document-dashboard.component.css']
})

export class DocumentDashboardComponent implements OnInit {
  documentData: Document[];
  navBarTitle: String = "My Documents";
  constructor() { }

  ngOnInit(): void {
    this.documentData = [
      {
          "crewId": "00019749",
          "documentType": "PASSPORT",
          "createdOn": "2007-12-03T10:15:30",
          "countryOfIssue": "US",
          "changedBy": "Michael",
          "lastName": "Williams",
          "firstName": "Michael",
          "middleName": "Junior",
          "dateOfBirth": "1983-12-17",
          "gender": "M",
          "subDocumentType": "",
          "documentNumber": "",
          "passportNumber": "56622990",
          "placeOfIssue": "US",
          "cityOfBirth": "Guyana",
          "stateOfBirth": "Guy",
          "countryOfBirth": "Guy",
          "dateOfIssue": "2019-05-24",
          "dateOfExpiration": "2028-05-23",
          "nationality": "US",
          "primaryPassport": "Y",
          "validIn": "",
          "entry": "",
          "dMSObjectId": "",
          "invalidData": "N"
      },
      {
          "crewId": "00019749",
          "documentType": "PASSPORT",
          "createdOn": "2023-07-14T10:15:30",
          "countryOfIssue": "CAN",
          "changedBy": "Michael",
          "lastName": "Williams",
          "firstName": "Michael",
          "middleName": "Junior",
          "dateOfBirth": "1983-12-17",
          "gender": "M",
          "subDocumentType": "",
          "documentNumber": "",
          "passportNumber": "12345678",
          "placeOfIssue": "Canada",
          "cityOfBirth": "Calgary",
          "stateOfBirth": "Cal",
          "countryOfBirth": "Cal",
          "dateOfIssue": "2019-05-24",
          "dateOfExpiration": "2028-05-23",
          "nationality": "US",
          "primaryPassport": "N",
          "validIn": "",
          "entry": "",
          "dMSObjectId": "",
          "invalidData": "N"
      },
      {
          "crewId": "00019749",
          "documentType": "PASSPORT",
          "createdOn": "2023-07-17T08:41:25",
          "countryOfIssue": "IN",
          "changedBy": "Michael",
          "lastName": "Williams",
          "firstName": "Michael",
          "middleName": "Junior",
          "dateOfBirth": "1983-12-17",
          "gender": "M",
          "subDocumentType": "",
          "documentNumber": "",
          "passportNumber": "56622990",
          "placeOfIssue": "US",
          "cityOfBirth": "Guyana",
          "stateOfBirth": "Guy",
          "countryOfBirth": "Guy",
          "dateOfIssue": "2019-05-24",
          "dateOfExpiration": "2028-05-23",
          "nationality": "US",
          "primaryPassport": "N",
          "validIn": "",
          "entry": "",
          "dMSObjectId": "-R6Qxsw3QVQ_VL9dmcYAIWgCcxmG7s9vhqzuWZfh-nc",
          "invalidData": "N"
      }
  ]
  }

}
