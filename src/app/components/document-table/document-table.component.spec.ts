import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PassportTableComponent } from './document-table.component';

describe('PassportTableComponent', () => {
  let component: PassportTableComponent;
  let fixture: ComponentFixture<PassportTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PassportTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PassportTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
