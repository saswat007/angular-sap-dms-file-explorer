import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
  @Input() searchPHText: String;
  inputSearchValue: String = '';
  @Output() searchValueEmitter = new EventEmitter<String>();

  constructor() { }

  ngOnInit(): void {
  }

  searchTextChange(){
    this.searchValueEmitter.emit(this.inputSearchValue);
  }
}
